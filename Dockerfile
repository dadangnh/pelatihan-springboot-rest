FROM adoptopenjdk/openjdk15:alpine-jre
WORKDIR /opt/app
COPY target/spring-rest-0.0.1-SNAPSHOT.jar springrest.jar
EXPOSE 8090
ENTRYPOINT ["java", "-jar", "springrest.jar"]
