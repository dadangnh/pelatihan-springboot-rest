package com.dadangnh.springdjp.dto;

public class SearchData {
    private String key;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
