package com.dadangnh.springdjp.services;

import com.dadangnh.springdjp.models.entities.Sessions;
import com.dadangnh.springdjp.models.entities.User;
import com.dadangnh.springdjp.models.repositories.SessionsRepository;
import com.dadangnh.springdjp.models.repositories.UserRepository;
import com.dadangnh.springdjp.utilities.MD5Generator;
import com.dadangnh.springdjp.utilities.UUIDGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;

@Service
@Transactional
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SessionsRepository sessionsRepository;

    public User save(User user) throws Exception {
        user.setPassword(MD5Generator.generate(user.getPassword()));
        return userRepository.save(user);
    }

    public User findById(Long id) {
        return userRepository.findById(id).get();
    }

    public Iterable<User> findAll() {
        return userRepository.findAll();
    }

    public User findByEmailAndPassword(String email, String password) {
        User user = null;
        try {
            user =  userRepository.findByEmailAndPassword(email, MD5Generator.generate(password));
            return user;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Sessions login (String email, String password) {
        User user = findByEmailAndPassword(email, password);
        if (null == user) {
            return null;
        }

        try {
            // kick all previous session for the user
            sessionsRepository.setActiveByUserId(false, user.getId());

            // create new sessions
            Sessions sessions = new Sessions();
            sessions.setSessionId(UUIDGenerator.generateUniqueKeysWithUUIDAndMessageDigest());
            sessions.setUser(user);
            sessions.setActive(true);
            return sessionsRepository.save(sessions);

        } catch (Exception e) {
            return null;
        }
    }

    public Sessions logout(String sessionId) {
        Sessions sessions = sessionsRepository.findBySessionId(sessionId);

        if (null != sessions) {
            sessions.setLogoutAt(new Date());
            sessions.setActive(false);
            return sessionsRepository.save(sessions);
        } else {
            return null;
        }
    }
}
