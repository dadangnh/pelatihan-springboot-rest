package com.dadangnh.springdjp.services;

import com.dadangnh.springdjp.models.entities.App;
import com.dadangnh.springdjp.models.repositories.AppRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class AppService implements UserDetailsService {

    @Autowired
    private AppRepository appRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return appRepository.findByUserName(username);
    }

    public App register(App app) {
        return appRepository.save(app);
    }

    public Iterable<App> findAll() {
        return appRepository.findAll();
    }
}
