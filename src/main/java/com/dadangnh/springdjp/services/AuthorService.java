package com.dadangnh.springdjp.services;

import com.dadangnh.springdjp.models.entities.Author;
import com.dadangnh.springdjp.models.repositories.AuthorRepository;
import com.dadangnh.springdjp.utilities.CSVHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class AuthorService {

    @Autowired
    private AuthorRepository authorRepository;

    public List<Author> save(MultipartFile file) {
        try {
            List<Author> authors = CSVHelper.csvToAuthor(file.getInputStream());
            if (authors != null) {
                return authorRepository.saveAll(authors);
            }
        } catch (Exception e) {
            throw new RuntimeException("failed to store csv data:" + e.getMessage());
        }
        return null;
    }
}
