package com.dadangnh.springdjp.utilities;

import com.dadangnh.springdjp.dto.ResponseData;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;

import java.util.ArrayList;
import java.util.List;

public class ErrorsParsing {
    public static List<String> parse(Errors errors) {
        List<String> messages = new ArrayList<>();
        for (ObjectError error : errors.getAllErrors()) {
            messages.add(error.getDefaultMessage());
        }
        return messages;
    }

    public static ResponseData checkErrorFromValidation(Errors errors) {
        ResponseData response = new ResponseData();

        if (errors.hasErrors()) {
            response.setStatus(false);
            response.setMessages(parse(errors));
        }

        return response;
    }

}
