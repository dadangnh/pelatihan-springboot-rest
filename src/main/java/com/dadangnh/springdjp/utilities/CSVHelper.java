package com.dadangnh.springdjp.utilities;

import com.dadangnh.springdjp.models.entities.Author;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class CSVHelper {
    public static String TYPE = "text/csv";

    public static boolean hasCSVFormat(MultipartFile file) {
        return TYPE.equals(file.getContentType());
    }

    public static List<Author> csvToAuthor(InputStream is) {
        try {
            List<Author> authors = new ArrayList<>();
            BufferedReader fileReader = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8));
            CSVParser csvParser = new CSVParser(
                    fileReader,
                    CSVFormat.DEFAULT
                                .withFirstRecordAsHeader()
                                .withIgnoreHeaderCase()
                                .withTrim()
            );

            Iterable<CSVRecord> records = csvParser.getRecords();

            for (CSVRecord record: records) {
                Author author = new Author();
                author.setEmail(record.get("Email"));
                author.setFullName(record.get("FullName"));
                authors.add(author);
            }

            csvParser.close();
            return authors;
        } catch (Exception e) {
            throw new RuntimeException("failed to parse csv file: " + e.getMessage());
        }
    }
}
