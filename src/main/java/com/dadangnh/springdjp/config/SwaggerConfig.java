package com.dadangnh.springdjp.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;
import java.util.List;

@Configuration
@EnableSwagger2
@Import(BeanValidatorPluginsConfiguration.class)
public class SwaggerConfig {

    private List<Parameter> globalParameterList(){
        Parameter params = new ParameterBuilder()
                .name("SESSIONID") // name of the header
                .modelRef(new ModelRef("string")) // data-type of the header
                .required(false) // required/optional
                .parameterType("header") // for query-param, this value can be 'query'
                .description("User Session Id")
                .build();
        return Collections.singletonList(params);
    }

    @Bean
    public Docket apiDocket(){
        return new Docket(DocumentationType.SWAGGER_2)
                .globalOperationParameters(globalParameterList())
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build();
    }

    public ApiInfo getApiInfo() {
        return new ApiInfo(
                "Book API",
                "Simple REST API for Book",
                "1.0",
                "https://www.dadangnh.com",
                new Contact(
                        "Dadang NH",
                        "https://www.dadangnh.com",
                        "contact@dadangnh.com"
                ),
                "MIT",
                "https://mit.license",
                Collections.emptyList()
        );
    }
}
