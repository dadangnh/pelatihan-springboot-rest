package com.dadangnh.springdjp.models.repositories;

import com.dadangnh.springdjp.models.entities.Author;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorRepository extends JpaRepository<Author, String> {
}
