package com.dadangnh.springdjp.models.repositories;

import com.dadangnh.springdjp.models.entities.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {
    User findByEmailAndPassword(String email, String password);
}
