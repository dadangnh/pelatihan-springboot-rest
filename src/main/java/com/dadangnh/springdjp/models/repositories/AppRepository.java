package com.dadangnh.springdjp.models.repositories;

import com.dadangnh.springdjp.models.entities.App;
import org.springframework.data.repository.CrudRepository;

public interface AppRepository extends CrudRepository<App, String> {
    App findByUserName(String userName);
}
