package com.dadangnh.springdjp.models.repositories;

import com.dadangnh.springdjp.models.entities.Book;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BookRepository extends PagingAndSortingRepository<Book, Long> {

    @Query("SELECT b FROM Book b WHERE b.title = :param")
    List<Book> cariBerdasarkanTitle(@Param("param") String title);

    List<Book> findByTitle(String title);

    List<Book> findByTitleLike(String title);

    List<Book> findByTitleContains(String title);

    List<Book> findByCategoryId(Long id);

    List<Book> findByAuthorId(Long id);
}
