package com.dadangnh.springdjp.models.repositories;

import com.dadangnh.springdjp.models.entities.Sessions;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface SessionsRepository extends CrudRepository<Sessions, Long> {

    Sessions findBySessionId(String sessionId);

    List<Sessions> findByUserId(Long userId);

    @Modifying
    @Query("UPDATE Sessions s SET s.active = :param1 WHERE s.user.id = :param2")
    int setActiveByUserId(@Param("param1") boolean active, @Param("param2") Long userId);
}
