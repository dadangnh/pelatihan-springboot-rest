package com.dadangnh.springdjp.models.repositories;

import com.dadangnh.springdjp.models.entities.Category;
import org.springframework.data.repository.CrudRepository;

public interface CategoryRepository extends CrudRepository<Category, Long> {

}
