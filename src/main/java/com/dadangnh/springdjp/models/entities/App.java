package com.dadangnh.springdjp.models.entities;

import com.dadangnh.springdjp.utilities.UUIDGenerator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;

@Entity
@Table(name = "tbl_apps")
public class App implements UserDetails {

    @Id
    private String id;

    @Column(length = 100, nullable = false, unique = true)
    private String appName;

    @Column(length = 100, nullable = false, unique = true)
    private String userName;

    @JsonIgnore
    @Column(length = 255, nullable = false, unique = false)
    private String password;

    @Column(length = 255, nullable = false, unique = true)
    private String appKey;

    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return AuthorityUtils.commaSeparatedStringToAuthorityList("APP_CLIENT");
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.userName;
    }

    // For learning, set default as true
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    // For learning, set default as true
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    // For learning, set default as true
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    // For learning, set default as true
    @Override
    public boolean isEnabled() {
        return true;
    }

    @PrePersist
    private void setInitEntity(){
        this.id = UUIDGenerator.generateType4UUID().toString();
        this.createdAt = new Date();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
