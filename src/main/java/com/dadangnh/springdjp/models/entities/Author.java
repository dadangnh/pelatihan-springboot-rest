package com.dadangnh.springdjp.models.entities;

import com.dadangnh.springdjp.utilities.UUIDGenerator;

import javax.persistence.*;

@Entity
@Table(name = "tbl_author")
public class Author {

    @Id
    private String id;

    @Column(length = 250, nullable = false)
    private String fullName;

    @Column(length = 100, nullable = false, unique = true)
    private String email;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @PrePersist
    public void setRandomID(){
        this.id = UUIDGenerator.generateType4UUID().toString();
    }

}
