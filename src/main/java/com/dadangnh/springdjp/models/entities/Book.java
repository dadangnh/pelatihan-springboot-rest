package com.dadangnh.springdjp.models.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "tbl_book")
public class Book {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @Column(length = 5, nullable = false, unique = true)
    private String code;

    @Column(length = 200, nullable = false)
    private String title;

    @Column(length = 255, nullable = true)
    private String description;

    @ManyToOne
    private Author author;

    @JsonIgnore
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    @ManyToOne
    private Category category;
    public Book() {}

    public Book(Long id, String code, String title, String description, Author author, Category category) {
        this.id = id;
        this.code = code;
        this.title = title;
        this.description = description;
        this.author = author;
        this.category = category;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        if (createdDate == null) {
            this.createdDate = new Date();
        } else {
            this.createdDate = createdDate;
        }
    }

    @PrePersist
    private void setAutoCreatedDate() {
        this.createdDate = new Date();
    }
}
