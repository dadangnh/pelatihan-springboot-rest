package com.dadangnh.springdjp.controllers;

import com.dadangnh.springdjp.dto.LoginData;
import com.dadangnh.springdjp.dto.ResponseData;
import com.dadangnh.springdjp.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/users/login")
public class LoginUserController {
    @Autowired
    private UserService userService;

    @PostMapping
    public ResponseEntity<ResponseData> login(@RequestBody LoginData login) {
        ResponseData response = new ResponseData();
        try {
            response.setPayload(userService.login(login.getEmail(), login.getPassword()));
            response.setStatus(true);
            response.getMessages().add("Login success");
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            response.setStatus(false);
            response.getMessages().add(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }
}
