package com.dadangnh.springdjp.controllers;

import com.dadangnh.springdjp.dto.RegisterUserData;
import com.dadangnh.springdjp.dto.ResponseData;
import com.dadangnh.springdjp.dto.ViewUserData;
import com.dadangnh.springdjp.models.entities.User;
import com.dadangnh.springdjp.services.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/users/register")
public class RegisterUserController {
    @Autowired
    private UserService userService;

    @Autowired
    private ModelMapper modelMapper;

    @PostMapping
    public ResponseEntity<ResponseData> register(@RequestBody RegisterUserData userData) {
        ResponseData response = new ResponseData();
        try {
            User user = modelMapper.map(userData, User.class);
            userService.save(user);

            ViewUserData viewUserData = modelMapper.map(user, ViewUserData.class);

            response.setPayload(viewUserData);
            response.setStatus(true);
            response.getMessages().add("User saved");
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            response.setStatus(false);
            response.getMessages().add(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }
}
