package com.dadangnh.springdjp.controllers;

import com.dadangnh.springdjp.models.entities.Category;
import com.dadangnh.springdjp.models.repositories.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/modules/categories")
public class CategoryController {

    @Autowired
    private CategoryRepository categoryRepository;

    @PostMapping
    public Category create(@RequestBody Category category) {
        return categoryRepository.save(category);
    }

    @GetMapping
    public Iterable<Category> findAll() {
        return categoryRepository.findAll();
    }
}
