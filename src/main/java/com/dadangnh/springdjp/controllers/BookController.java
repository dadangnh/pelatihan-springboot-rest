package com.dadangnh.springdjp.controllers;

import com.dadangnh.springdjp.dto.BookData;
import com.dadangnh.springdjp.dto.ResponseData;
import com.dadangnh.springdjp.dto.SearchData;
import com.dadangnh.springdjp.models.entities.Book;
import com.dadangnh.springdjp.models.repositories.AuthorRepository;
import com.dadangnh.springdjp.models.repositories.BookRepository;
import com.dadangnh.springdjp.models.repositories.CategoryRepository;
import com.dadangnh.springdjp.utilities.ErrorsParsing;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/modules/books")
public class BookController {

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private AuthorRepository authorRepository;

    @PostMapping
    public ResponseEntity<ResponseData> createBook(@Valid @RequestBody BookData book, Errors errors) {
        ResponseData response = ErrorsParsing.checkErrorFromValidation(errors);

        if (!response.isStatus()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }

        try {
            Book newBook = new Book();
            newBook.setTitle(book.getTitle());
            newBook.setDescription(book.getDescription());
            newBook.setAuthor(authorRepository.findById(book.getAuthorId()).get());
            newBook.setCode(book.getCode());
            newBook.setCategory(categoryRepository.findById(book.getCategoryId()).get());

            response.setPayload(bookRepository.save(newBook));
            response.setStatus(true);
            response.getMessages().add("Book saved!");
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            response.setStatus(false);
            response.getMessages().add(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }

    @GetMapping("/{page}/{size}")
    public ResponseEntity<ResponseData> findAll(@PathVariable("page") int page, @PathVariable("size") int size) {
        ResponseData response = new ResponseData();

        try {
            Pageable pageable = PageRequest.of(page-1, size, Sort.by("title").descending());
            response.setPayload(bookRepository.findAll(pageable));
            response.setStatus(true);
            response.getMessages().add("All books retrieved");
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            response.setStatus(false);
            response.getMessages().add(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseData> findOne(@PathVariable("id") Long id) {
        ResponseData response = new ResponseData();

        try {
            Optional<Book> book = bookRepository.findById(id);
            if (book.isEmpty()) {
                response.setStatus(false);
                response.getMessages().add("Book with id " + id + " not found");
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
            }
            response.setPayload(book);
            response.setStatus(true);
            response.getMessages().add("A Book retrieved");
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            response.setStatus(false);
            response.getMessages().add(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }

    @PostMapping("/search")
    public ResponseEntity<ResponseData> findByTitle(@RequestBody SearchData searchData) {
        ResponseData response = new ResponseData();

        try {
            List<Book> books = bookRepository.findByTitleContains(searchData.getKey());
            if (books.size() < 1) {
                response.setStatus(false);
                response.getMessages().add("Book with title " + searchData.getKey() + " not found");
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
            }
            response.setPayload(books);
            response.setStatus(true);
            response.getMessages().add("Book found");
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            response.setStatus(false);
            response.getMessages().add(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }


    @PutMapping("/{id}")
    public ResponseEntity<ResponseData> update(@PathVariable("id") Long id, @Valid @RequestBody BookData book,
                                               Errors errors
    ) {
        ResponseData response = ErrorsParsing.checkErrorFromValidation(errors);

        if (!response.isStatus()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }

        try {
            Optional<Book> existingBook = bookRepository.findById(id);
            if (existingBook.isEmpty()) {
                response.setStatus(false);
                response.getMessages().add("Book with id " + id + " not found");
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
            }
            existingBook.get().setTitle(book.getTitle());
            existingBook.get().setDescription(book.getDescription());
            existingBook.get().setAuthor(authorRepository.findById(book.getAuthorId()).get());
            existingBook.get().setCode(book.getCode());
            existingBook.get().setCategory(categoryRepository.findById(book.getCategoryId()).get());
            bookRepository.save(existingBook.get());

            response.setPayload(existingBook.get());
            response.setStatus(true);
            response.getMessages().add("Book updated!");
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            response.setStatus(false);
            response.getMessages().add(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ResponseData> removeBook(@PathVariable("id") Long id) {
        ResponseData response = new ResponseData();

        try {
            Optional<Book> existingBook = bookRepository.findById(id);
            if (existingBook.isEmpty()) {
                response.setStatus(false);
                response.getMessages().add("Book with id " + id + " not found");
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
            }
            bookRepository.delete(existingBook.get());
            response.setStatus(true);
            response.getMessages().add("Book with id " + id + " deleted");
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            response.setStatus(false);
            response.getMessages().add(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }


    @GetMapping("/category/{id}")
    public ResponseEntity<ResponseData> findByCategory(@PathVariable("id") Long id) {
        ResponseData response = new ResponseData();

        try {
            List<Book> books = bookRepository.findByCategoryId(id);
            if (books.size() < 1) {
                response.setStatus(false);
                response.getMessages().add("Book with category id " + id + " not found");
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
            }
            response.setPayload(books);
            response.setStatus(true);
            response.getMessages().add("Book found");
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            response.setStatus(false);
            response.getMessages().add(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }

}
