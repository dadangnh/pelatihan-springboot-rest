package com.dadangnh.springdjp.controllers;

import com.dadangnh.springdjp.dto.ResponseData;
import com.dadangnh.springdjp.models.entities.Author;
import com.dadangnh.springdjp.models.repositories.AuthorRepository;
import com.dadangnh.springdjp.services.AuthorService;
import com.dadangnh.springdjp.utilities.CSVHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/api/modules/author")
public class AuthorController {

    @Autowired
    private AuthorRepository authorRepository;

    @Autowired
    private AuthorService authorService;

    @PostMapping
    public Author create(@RequestBody Author author) {
        return authorRepository.save(author);
    }

    @GetMapping
    public Iterable<Author> findAll() {
        return authorRepository.findAll();
    }

    public ResponseEntity<ResponseData> uploadFile(@RequestParam("file") MultipartFile file) {
        ResponseData response = new ResponseData();

        if (CSVHelper.hasCSVFormat(file)) {
            try {
                List<Author> author = authorService.save(file);
                response.setStatus(true);
                response.setPayload(author);
                response.getMessages().add("Upload Success!");
                return ResponseEntity.ok(response);
            } catch (Exception e) {
                response.setStatus(false);
                response.getMessages().add(e.getMessage());
                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(response);
            }
        }

        response.setStatus(false);
        response.getMessages().add("Please upload csv file");
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
    }
}
