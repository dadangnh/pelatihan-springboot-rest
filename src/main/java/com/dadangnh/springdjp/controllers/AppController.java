package com.dadangnh.springdjp.controllers;

import com.dadangnh.springdjp.dto.ResponseData;
import com.dadangnh.springdjp.models.entities.App;
import com.dadangnh.springdjp.services.AppService;
import com.dadangnh.springdjp.utilities.UUIDGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Base64;

@RestController
@RequestMapping("/api/apps")
public class AppController {

    @Autowired
    private AppService appService;

    @PostMapping
    public ResponseEntity<ResponseData> registerApp(@RequestBody App app) {
        ResponseData response = new ResponseData();
        try {
            app.setPassword(UUIDGenerator.generateUniqueKeysWithUUIDAndMessageDigest());
            app.setAppKey(generateAppKey(app.getUserName(), app.getPassword()));
            app = appService.register(app);
            response.setStatus(true);
            response.getMessages().add("App Saved");
            response.setPayload(app);
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            response.setStatus(false);
            response.getMessages().add(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);

        }
    }

    private ResponseEntity<ResponseData> findAll() {
        ResponseData response = new ResponseData();
        response.setStatus(true);
        response.setPayload(appService.findAll());
        return ResponseEntity.ok(response);
    }

    private String generateAppKey(String userName, String password) {
        String baseStr = userName + ":" + password;
        return Base64.getEncoder().encodeToString(baseStr.getBytes());
    }
}
