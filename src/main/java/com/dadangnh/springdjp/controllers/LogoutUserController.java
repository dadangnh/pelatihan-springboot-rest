package com.dadangnh.springdjp.controllers;

import com.dadangnh.springdjp.dto.ResponseData;
import com.dadangnh.springdjp.models.entities.Sessions;
import com.dadangnh.springdjp.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api/modules/users/logout")
public class LogoutUserController {
    @Autowired
    private UserService userService;

    @Autowired
    private HttpServletRequest request;

    @GetMapping
    public ResponseEntity<ResponseData> logout() {
        ResponseData response = new ResponseData();
        try {
            Sessions sessions = userService.logout(request.getHeader("SESSIONID"));
            response.setPayload(sessions);
            response.setStatus(true);
            response.getMessages().add("User logout");
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            response.setStatus(false);
            response.getMessages().add(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }
}
