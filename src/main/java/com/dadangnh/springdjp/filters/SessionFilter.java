package com.dadangnh.springdjp.filters;

import com.dadangnh.springdjp.models.entities.Sessions;
import com.dadangnh.springdjp.models.repositories.SessionsRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Component
@Order(2)
public class SessionFilter implements Filter {

    @Autowired
    private SessionsRepository sessionsRepository;


    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException
    {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        ObjectMapper mapper = new ObjectMapper();

        String requestPath = req.getRequestURI();
        if (requestPath.startsWith("/api/modules")) {
            String sessionId = req.getHeader("SESSIONID");
            if (null != sessionId) {
                // check sessionId
                Sessions sessions = sessionsRepository.findBySessionId(sessionId);
                if (null == sessions) {
                    // if no session found, do login
                    Map<String, Object> errors = new HashMap<>();
                    errors.put("status", false);
                    errors.put("messages", new String[]{"Please login first"});
                    errors.put("payload", null);
                    res.setStatus(HttpStatus.FORBIDDEN.value());
                    res.setContentType(MediaType.APPLICATION_JSON_VALUE);
                    mapper.writeValue(res.getWriter(), errors);
                } else {
                    // session expired
                    if (!sessions.isActive()) {
                        Map<String, Object> errors = new HashMap<>();
                        errors.put("status", false);
                        errors.put("messages", new String[]{"SESSIONID expired"});
                        errors.put("payload", null);
                        res.setStatus(HttpStatus.FORBIDDEN.value());
                        res.setContentType(MediaType.APPLICATION_JSON_VALUE);
                        mapper.writeValue(res.getWriter(), errors);
                    } else {
                        // login valid
                        chain.doFilter(req, res);
                    }
                }
            } else {
                // if no session header found
                Map<String, Object> errors = new HashMap<>();
                errors.put("status", false);
                errors.put("messages", new String[]{"Please provide SESSIONID Header"});
                errors.put("payload", null);
                res.setStatus(HttpStatus.FORBIDDEN.value());
                res.setContentType(MediaType.APPLICATION_JSON_VALUE);
                mapper.writeValue(res.getWriter(), errors);
            }
        } else {
            chain.doFilter(req, res);
        }

        // handling after response here
    }
}
